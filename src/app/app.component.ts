import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'front-exemple';

  weather;

  constructor(private httpClient: HttpClient) {
    this.httpClient.get(`http://localhost:8088/weatherforecast`).subscribe(
      data => {
        this.weather = data;
      }
    );
  }
}
