FROM node:alpine AS builder
COPY . ./front-exemple
WORKDIR /front-exemple
RUN npm i
RUN $(npm bin)/ng build --prod

FROM nginx:1.15.8-alpine
COPY --from=builder /front-exemple/dist/front-exemple/ /usr/share/nginx/html